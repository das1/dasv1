var express = require('express');
var router = express.Router();
var dasController = require('../controllers/dasAuthController');

router.post("/getStudentFpScanData", dasController.getStudentFpScan);
router.post("/registerStudent", dasController.registerStudent);
router.post("/studentAttendance", dasController.studentAttendance);
router.post("/sync", dasController.sync);


module.exports = router;